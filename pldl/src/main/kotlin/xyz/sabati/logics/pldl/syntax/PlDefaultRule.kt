package xyz.sabati.logics.pldl.syntax

import net.sf.tweety.commons.Signature
import net.sf.tweety.logics.pl.syntax.PropositionalFormula

/**
 * Represent an atomic default rule ins propositional logic.
 * @author  Samuel Batissou
 *
 * @property pre prerequisites
 * @property jus justifications
 * @property conc conclusion
 * @property signature signature of the language
 */
class PlDefaultRule(
    val pre: PropositionalFormula,
    val jus: Collection<PropositionalFormula>,
    val conc: PropositionalFormula
) {
    val signature: Signature
        get() {
            val result = pre.signature
            result.addSignature(conc.signature)
            for (f in jus)
                result.addSignature(f.signature)
            return result
        }

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        if (javaClass != other?.javaClass)
            return false

        other as PlDefaultRule
        if (jus != other.jus)
            return false
        if (pre != other.pre)
            return false
        if (conc != other.conc)
            return false

        return true
    }

    override fun hashCode(): Int {
        var result = pre.hashCode()
        result = 31 * result + jus.hashCode()
        result = 31 * result + conc.hashCode()
        return result
    }

}