package xyz.sabati.logics.pldl.semactics

import net.sf.tweety.logics.pl.syntax.PropositionalFormula

/**
 * Represent a default extension.
 * @author Samuel Batissou
 *
 * @property formulae collection of formula.
 */
class PlExtension{
    val formulae : MutableCollection<PropositionalFormula>

    constructor() : this(mutableListOf())
    constructor(formulae: Collection<PropositionalFormula>){
        this.formulae = mutableSetOf()
        this.formulae.addAll(formulae)
    }

    override fun toString(): String {
        return "PlExtension(formulae=$formulae)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PlExtension

        if (formulae != other.formulae) return false

        return true
    }

    override fun hashCode(): Int {
        return formulae.hashCode()
    }
}