package xyz.sabati.logics.pldl.syntax

import net.sf.tweety.commons.BeliefBase
import net.sf.tweety.commons.Signature
import net.sf.tweety.logics.pl.syntax.PlBeliefSet
import xyz.sabati.logics.pldl.syntax.PlDefaultRule

/**
 * Models a default propositional theory.
 * @author  Samuel Batissou
 */
class PlDefaultTheory : BeliefBase {
    val plDefaultFormulae: Collection<PlDefaultRule>
    val facts: PlBeliefSet

    constructor(plDefaultFormulae: Collection<PlDefaultRule>, facts: PlBeliefSet) {
        this.plDefaultFormulae = plDefaultFormulae
        this.facts = facts
    }

    override fun getSignature(): Signature {
        val result = facts.signature
        for (d in plDefaultFormulae)
            result.addSignature(d.signature)
        return result
    }
}