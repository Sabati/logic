package xyz.sabati.logics.pldl.semactics

import xyz.sabati.logics.pldl.syntax.PlDefaultRule
import xyz.sabati.logics.pldl.syntax.PlDefaultTheory
import net.sf.tweety.logics.pl.reasoner.SatReasoner
import net.sf.tweety.logics.pl.syntax.Negation
import net.sf.tweety.logics.pl.syntax.PlBeliefSet
import net.sf.tweety.logics.pl.syntax.PropositionalFormula

/**
 * Sequence of defaults
 * @property defaults sequence of defaults
 * @property out the out set
 * @property input the input belief
 * @property process <=> all defaults are unique and applicable in sequence
 * @author Samuel Batissou
 */
class PlDefaultSequence {
    val defaults: MutableCollection<PlDefaultRule>
    val out: MutableSet<PropositionalFormula>
    val input: PlBeliefSet
    var process = true

    init {
        defaults = mutableListOf()
        out = mutableSetOf()
        input = PlBeliefSet()
    }

    /**
     * constructs an empty sequence of defaults of the default throry <<defaultTheory>>.
     * @param defaultTheory from which defaults will be added to the sequence.
     */
    constructor(defaultTheory: PlDefaultTheory) {
        input.addAll(defaultTheory.facts)
    }

    /**
     * constructs a new sequence by appliyng a plDefaultRule to a defaultSequence.
     * @param currentSequence
     * @param ruleToApply
     */
    private constructor(currentSequence: PlDefaultSequence, ruleToApply: PlDefaultRule) {
        defaults.addAll(currentSequence.defaults)
        input.addAll(currentSequence.input)
        process = currentSequence.isApplicable(ruleToApply)
        for (r in defaults)
            if (ruleToApply == r)
                process = false
        input.add(ruleToApply.conc) // add conclusion of the rule to the input
        defaults.add(ruleToApply) // add the default  rule
        out.addAll(currentSequence.out) // copy the out
        for (f in ruleToApply.jus) // add the justifications to the out
            out.add(Negation(f))
    }

    /**
     * Construct a new PlDefaultSequence.
     * @param plDefaultRule
     * @return new PlDefaultSequence
     */
    fun apply(plDefaultRule: PlDefaultRule): PlDefaultSequence {
        return PlDefaultSequence(this, plDefaultRule)
    }


    /**
     * Test if the default rule is applicable for his justifications,
     * and if it is then test if the precondition is fulfilled.
     * @param plDefaultRule the rule to try
     * @return true iff plDefaultRule is applicable to inputs
     */
    fun isApplicable(plDefaultRule: PlDefaultRule): Boolean {
        val reasoner = SatReasoner()
        for (f in plDefaultRule.jus) {
            if (reasoner.query(input, Negation(f)))
                return false
        }
        return reasoner.query(input, plDefaultRule.pre)
    }

    /**
     * @return true if there is no x: x in input and no x in out
     */
    fun isSuccessful(): Boolean {
        val reasoner = SatReasoner()
        for (f in out) {
            if (reasoner.query(input, f))
                return false
        }
        return true
    }

    /**
     * Test whether all applicable defaults from t have been applied
     * @return true iff every possible default is applied
     */
    fun isClosed(plDefaultTheory: PlDefaultTheory): Boolean {
        for (d in plDefaultTheory.plDefaultFormulae)
            if (this.apply(d).process)
                return false
        return true
    }
}