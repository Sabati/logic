package xyz.sabati.logics.pldl.semactics

import net.sf.tweety.logics.pl.sat.Sat4jSolver
import net.sf.tweety.logics.pl.sat.SatSolver
import net.sf.tweety.logics.pl.syntax.*
import net.sf.tweety.logics.rdl.syntax.DefaultRule
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import xyz.sabati.logics.pldl.syntax.PlDefaultRule
import xyz.sabati.logics.pldl.util.PlDefaultTheoryBuilder

internal class PlDefaultProcessTreeTest {

    companion object {
        @BeforeAll
        @JvmStatic
        fun setup() {
            SatSolver.setDefaultSolver(Sat4jSolver())
        }
    }

    @Test
    fun getExtensions() {
        val build = PlDefaultTheoryBuilder()
        build.addFact(Proposition("a"))
        build.addDefaultRule(
            PlDefaultRule(
                Proposition("a"),
                listOf(Negation(Proposition("b"))),
                Proposition("c")
            )
        )
        build.addDefaultRule(
            PlDefaultRule(
                Tautology(),
                listOf(Negation(Proposition("c"))),
                Proposition("d")
            )
        )
        build.addDefaultRule(
            PlDefaultRule(
                Tautology(),
                listOf(Negation(Proposition("d"))),
                Proposition("e")
            )
        )

        val res = PlDefaultProcessTree(build.build()).extensions

        val expected = listOf(
            PlExtension(
                listOf(
                    Proposition("a"),
                    Proposition("c"),
                    Proposition("e")
                )
            )
        )
        assertEquals(expected.toSet(),res.toSet())
    }
}