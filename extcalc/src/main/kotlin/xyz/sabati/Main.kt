package xyz.sabati

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import net.sf.tweety.logics.pl.sat.Sat4jSolver
import net.sf.tweety.logics.pl.sat.SatSolver
import xyz.sabati.logics.pldl.parser.PlDlParser
import xyz.sabati.logics.pldl.semactics.PlDefaultProcessTree
import xyz.sabati.logics.pldl.semactics.PlExtension
import java.nio.file.Files
import java.nio.file.Paths

fun PlExtension.toSpeString(): String {
    return this.formulae.joinToString(", ")
}

class Extcalc : CliktCommand() {
    private val oneExtension by
    option("--oneExtension", "-e", help = "Prints one extension from the file").flag()
    private val allExtensions by
    option("--allExtensions", "-a", help = "Prints all the extension from the file").flag()

    private val paths by argument("PATH", help = "Path to the file with the KB").multiple()

    init {
        SatSolver.setDefaultSolver(Sat4jSolver())
    }

    override fun run() {
        for (path in paths) {
            val reader = Files.newBufferedReader(Paths.get(path))
            val dt = PlDlParser().parseBeliefBase(reader)
            val extensions = PlDefaultProcessTree(dt).extensions

            if (oneExtension)
                echo(extensions.first().toSpeString())
            if (allExtensions)
                extensions.toSet().forEach { echo(it.toSpeString()) }
        }
    }
}

fun main(args: Array<String>) = Extcalc().main(args)
