#!/bin/bash

echo "Building th project"
mvn package > /dev/null
echo "Building the ktdoc"
cd pldl
mvn dokka:dokka > /dev/null

echo "jar can be found in logic/extcalc/target/"
echo "the javadoc can be found in logic:pldl/target/dokka/"
echo "or https://samblaise.gitlab.io/logic/"
